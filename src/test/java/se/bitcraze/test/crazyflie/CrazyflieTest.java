/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.test.crazyflie;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.bitcraze.crazyflie.ConnectionListener;
import se.bitcraze.crazyflie.Crazyflie;
import se.bitcraze.crazyflie.LogListener;
import se.bitcraze.crazyflie.Logging;
import se.bitcraze.crazyflie.crtp.CrtpDriver;

/**
 * @author Andreas Huber
 * 
 */
public class CrazyflieTest {

	public static final Logger log = LoggerFactory
			.getLogger(CrazyflieTest.class);

	public CrazyflieTest() {

	}

	public void start() throws Exception {
		String[] uris = Crazyflie.scanInterfaces();
		if (ArrayUtils.isEmpty(uris)) {
			log.error("No Interfaces are found");
			return;
		}
		log.info("Found: {}", Arrays.toString(uris));
		Crazyflie crazyflie = new Crazyflie();
		Logging logging = crazyflie.getLogging();
		crazyflie.addListener(new LogListener() {

			@Override
			public void valuesReceived(String name, Map<String, Object> values) {
				log.info("{}", values.toString());
			}
		}, "stabilizer", "baro");
		crazyflie.getLogging().addGroup("stabilizer", 200, "stabilizer.roll",
				"stabilizer.pitch", "stabilizer.yaw", "stabilizer.thrust");
		crazyflie.getLogging().addGroup("baro", 200, "baro.aslLong",
				"baro.temp", "baro.pressure");
		logging.startAll();
		crazyflie.addListener(new ConnectionListener() {

			@Override
			public void linkQualityUpdate(CrtpDriver l, int quality) {
				// log.info("linkQualityUpdate: {}", quality);
			}

			@Override
			public void disconnected(CrtpDriver l) {
				log.info("disconnected");
			}

			@Override
			public void connectionSetupFinished(CrtpDriver l) {
				log.info("connectionSetupFinished");
			}

			@Override
			public void connectionLost(CrtpDriver l) {
				log.info("connectionLost");

			}

			@Override
			public void connectionInitiated(CrtpDriver l) {
				log.info("connectionInitiated");

			}

			@Override
			public void connectionFailed(CrtpDriver l) {
				log.info("connectionFailed");

			}
		});
		try {
			crazyflie.connect(uris[0]);

			while (!Thread.currentThread().isInterrupted())
				Thread.sleep(1000);
		} finally {
			crazyflie.disconnect();
		}
	}

	public static void main(String[] args) throws Exception {
		CrazyflieTest usb = new CrazyflieTest();
		usb.start();
	}
}
