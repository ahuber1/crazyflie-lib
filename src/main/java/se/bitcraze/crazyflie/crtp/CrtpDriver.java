/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie.crtp;

import se.bitcraze.crazyflie.ConnectionListener;

/**
 * Representation of a link to the Crazyflie.
 */
public interface CrtpDriver {

	/**
	 * Connect to the Crazyflie.
	 */
	public void connect() throws Exception;

	/**
	 * Disconnect from the Crazyflie.
	 */
	public void disconnect() throws Exception;

	/**
	 * Check whether the link is connected.
	 * 
	 * @return <code>true</code> if the link is connected.
	 */
	public boolean isConnected();

	/**
	 * Send data to the Crazyflie.
	 * 
	 * @param p
	 *            the packet of data to send.
	 */
	public void sendAsync(Crtp.Request request);

	public <T extends Crtp.Response> T send(Crtp.Request request);

	/**
	 * Add a listener to receive notifications about the connection status.
	 * 
	 * @param l
	 *            the listener to add.
	 */
	public void addListener(ConnectionListener l);

	/**
	 * Remote a previously registered connection listener. If the listener has
	 * not been registered before, nothing is done.
	 * 
	 * @param l
	 *            the listener to remove.
	 */
	public void removeListener(ConnectionListener l);

	/**
	 * Add a listener to receive notifications about incoming data.
	 * 
	 * @param l
	 *            the listener to add.
	 */
	public void addListener(DataListener l);

	/**
	 * Remote a previously registered data listener. If the listener has not
	 * been registered before, nothing is done.
	 * 
	 * @param l
	 *            the listener to remove.
	 */
	public void removeListener(DataListener l);

}
