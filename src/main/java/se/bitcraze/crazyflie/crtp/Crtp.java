/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie.crtp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.bitcraze.crazyflie.LogVariable;
import se.bitcraze.crazyflie.util.BytePrinter;

/**
 * 
 * Contains all the Crazyflie Protocol stuff
 * 
 * @author Andreas Huber
 * 
 */
public class Crtp {

	private static final Logger log = LoggerFactory.getLogger(Crtp.class);
	public static final char MOTOR_START = (char) 10001;
	public static final char MAX_THRUST = (char) 650000;
	public static final byte CONSOLE_PORT = 0x00;
	public static final byte PARAM_PORT = 0x02;
	public static final byte COMMAND_PORT = 0x03;
	public static final byte LOG_PORT = 0x05;
	public static final byte DEBUGDRIVER_PORT = 0x0E;
	public static final byte LINKCTRL_PORT = 0x0F;

	public static final byte LOG_CHANNEL_TOC = 0x00;
	public static final byte LOG_CHANNEL_SETTINGS = 0x01;
	public static final byte LOG_CHANNEL_DATA = 0x02;

	public static final byte PARAM_CHANNEL_TOC = 0x00;
	public static final byte PARAM_CHANNEL_READ = 0x01;
	public static final byte PARAM_CHANNEL_WRITE = 0x02;

	public static final Charset CHARSET = Charset.forName("US-ASCII");
	public static final byte SEPARATOR = 0x00;
	/**
	 * Byte order used when serializing/deserializing packets.
	 */
	public static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;
	/**
	 * NULL packet. Header is 0xFF without any data.
	 */
	public static final Request NULL_PACKET = new Request((byte) 0xFF) {
		@Override
		protected void serializeData(ByteBuffer buffer) {
		}

		@Override
		protected int getLength() {
			return 0;
		}
	};

	/**
	 * 
	 */
	private Crtp() {
	}

	public static final Response parseResponse(byte[] data) {
		byte port = (byte) (data[0] >> 4);
		switch (port) {
		case CONSOLE_PORT:
			return new ConsoleResponse(data);
		case LOG_PORT:
			return parseLogResponse(data);
		case PARAM_PORT:
			return parseParamResponse(data);
		default:
			break;
		}
		if (log.isInfoEnabled())
			log.info("Unknown response: {}", BytePrinter.printHex(data));
		return null;
	}

	private static final Response parseLogResponse(byte[] data) {
		byte channel = (byte) (data[0] & 0x03);
		switch (channel) {
		case LOG_CHANNEL_TOC:
			byte cmd = data[1];
			if (cmd == LogTOCItemResponse.CMD)
				return new LogTOCItemResponse(data);
			else if (cmd == LogTOCResponse.CMD)
				return new LogTOCResponse(data);
			break;
		case LOG_CHANNEL_SETTINGS:
			return new LogSettingsResponse(data);
		case LOG_CHANNEL_DATA:
			return new LogBlockResponse(data);
		default:
			break;
		}

		if (log.isInfoEnabled())
			log.info("Unknown Log Response: {}", BytePrinter.printHex(data));
		return null;
	}

	private static final Response parseParamResponse(byte[] data) {
		byte channel = (byte) (data[0] & 0x03);
		switch (channel) {
		case PARAM_CHANNEL_TOC:
			byte cmd = data[1];
			if (cmd == ParamTOCRequest.ITEM)
				return new ParamTOCItemResponse(data);
			else if (cmd == ParamTOCRequest.CRC)
				return new ParamTOCResponse(data);
			break;
		case PARAM_CHANNEL_READ:
			if (log.isInfoEnabled())
				log.info("Param read response: {}", BytePrinter.printHex(data));
			return new ParamReadResponse(data);
		case PARAM_CHANNEL_WRITE:
			if (log.isInfoEnabled())
				log.info("Param write response: {}", BytePrinter.printHex(data));
			return new ParamWriteResponse(data);
		default:
			break;
		}
		if (log.isInfoEnabled())
			log.info("Unknown Param Response: {}", BytePrinter.printHex(data));
		return null;
	}

	/**
	 * Packet of data which can be sent/received from/to the Crazyflie. All
	 * packet implementations must be immutable to avoid issues with modifying
	 * packets via references, e.g. in a send queue.
	 */
	public static abstract class Request {

		private final byte mPacketHeader;
		private byte[] mSerializedPacket;

		/**
		 * Create a new packet.
		 * 
		 * @param channel
		 *            channel to set in the header.
		 * @param port
		 *            port to set in the header.
		 */
		public Request(byte channel, byte port) {
			this((byte) (((port & 0x0F) << 4) | (channel & 0x03)));
		}

		/**
		 * Create a new packet.
		 * 
		 * @param packetHeader
		 *            header of the packet.
		 */
		public Request(byte packetHeader) {
			this.mPacketHeader = packetHeader;
			this.mSerializedPacket = null;
		}

		/**
		 * Get the header of the packet.
		 * 
		 * @return the header of the packet.
		 */
		public byte getHeader() {
			return mPacketHeader;
		}

		/**
		 * Serialize the data of the packet. Must not include the header.
		 * 
		 * @param buffer
		 *            the target buffer for serialization.
		 */
		protected abstract void serializeData(ByteBuffer buffer);

		/**
		 * Get the number of bytes used when serializing the data.
		 * 
		 * @return number of bytes required by the serialized data.
		 */
		protected abstract int getLength();

		/**
		 * Convert the packet to a byte array suitable for transmission.
		 * 
		 * @return byte array containing the header and packet data.
		 */
		public byte[] toByteArray() {
			// if it's the first call, serialize the packet and cache it
			if (mSerializedPacket == null) {
				ByteBuffer buffer = ByteBuffer.allocate(getLength() + 1).order(
						BYTE_ORDER);
				buffer.put(mPacketHeader);
				serializeData(buffer);
				mSerializedPacket = buffer.array();
			}

			return mSerializedPacket;
		}
	}

	public static abstract class Response {
		private final byte port;
		private final byte channel;

		private Response(byte[] data) {
			port = (byte) (data[0] >> 4);
			channel = (byte) (data[0] & 0x03);
		}

		/**
		 * @return the port
		 */
		public byte getPort() {
			return port;
		}

		/**
		 * @return the channel
		 */
		public byte getChannel() {
			return channel;
		}

		/**
		 * @return
		 */
		public abstract boolean isNotification();

	}

	public static class ParamTOCRequest extends Request {

		public static final byte ITEM = 0x00;
		public static final byte CRC = 0x01;
		private final byte command;
		private final byte item;
		private final byte length;

		public ParamTOCRequest() {
			super(PARAM_CHANNEL_TOC, PARAM_PORT);
			this.command = CRC;
			this.item = 0;
			length = 1;
		}

		/**
		 * Create a new Log TOC item Request
		 * 
		 * @param item
		 */
		public ParamTOCRequest(byte item) {
			super(PARAM_CHANNEL_TOC, PARAM_PORT);
			this.command = ITEM;
			this.item = item;
			length = 2;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Packet#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(command);
			if (length > 1)
				buffer.put(item);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Packet#getDataByteCount()
		 */
		@Override
		protected int getLength() {
			return length;
		}
	}

	public static final class ParamReadRequest extends Request {
		private final byte itemId;

		public ParamReadRequest(int itemId) {
			super(PARAM_CHANNEL_READ, PARAM_PORT);
			this.itemId = (byte) itemId;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Packet#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(itemId);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Packet#getDataByteCount()
		 */
		@Override
		protected int getLength() {
			return 1;
		}

	}

	public static final class ParamReadResponse extends Response {

		private final byte itemId;
		private final byte[] value;

		public ParamReadResponse(byte[] data) {
			super(data);
			itemId = data[1];
			this.value = Arrays.copyOfRange(data, 2, data.length);
		}

		/**
		 * @return the itemId
		 */
		public byte getItemId() {
			return itemId;
		}

		/**
		 * @return the data
		 */
		public byte[] getValue() {
			return value;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ParamReadResponse [value=" + Arrays.toString(value) + "]";
		}

	}

	public static final class ParamWriteRequest extends Request {
		private final byte itemId;
		private byte[] data;

		public ParamWriteRequest(int itemId, byte[] data) {
			super(PARAM_CHANNEL_WRITE, PARAM_PORT);
			this.itemId = (byte) itemId;
			this.data = data;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(itemId);
			buffer.put(data);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 1 + data.length;
		}
	}

	public static final class ParamWriteResponse extends Response {

		private final byte itemId;
		private final byte[] value;

		public ParamWriteResponse(byte[] data) {
			super(data);
			this.itemId = data[1];
			this.value = Arrays.copyOfRange(data, 2, data.length);
		}

		/**
		 * @return the itemId
		 */
		public byte getItemId() {
			return itemId;
		}

		/**
		 * @return the data
		 */
		public byte[] getValue() {
			return value;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ParamReadResponse [value=" + Arrays.toString(value) + "]";
		}

	}

	public static class ParamTOCItemResponse extends Response {
		public static final byte CMD = 0x00;
		private final byte item;
		private final byte type;
		private final String group;
		private final String name;
		private final boolean readOnly;

		/**
		 * @param data
		 */
		public ParamTOCItemResponse(byte[] data) {
			super(data);
			this.item = data[2];
			this.type = (byte) (data[3] & 0x0F);
			this.readOnly = (data[3] & 0x40) != 0;
			int idx = ArrayUtils.indexOf(data, SEPARATOR, 4);
			group = new String(data, 4, idx - 4);
			name = new String(data, idx + 1, data.length - idx - 2);
		}

		/**
		 * @return the item
		 */
		public byte getItem() {
			return item;
		}

		/**
		 * @return the type
		 */
		public byte getType() {
			return type;
		}

		/**
		 * @return the readOnly
		 */
		public boolean isReadOnly() {
			return readOnly;
		}

		/**
		 * @return the group
		 */
		public String getGroup() {
			return group;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ParamTOCItemResponse [item=" + item + ", type=" + type
					+ ", group=" + group + ", name=" + name + ", readOnly="
					+ readOnly + "]";
		}

	}

	public static final class ParamTOCResponse extends Response {

		private final byte paramCount;
		private byte[] crc32;

		/**
		 * 
		 */
		public ParamTOCResponse(byte[] data) {
			super(data);
			paramCount = data[2];
			crc32 = Arrays.copyOfRange(data, 3, 6);
		}

		/**
		 * @return the crc32
		 */
		public byte[] getCrc32() {
			return crc32;
		}

		/**
		 * @param crc32
		 *            the crc32 to set
		 */
		public void setCrc32(byte[] crc32) {
			this.crc32 = crc32;
		}

		/**
		 * @return the paramCount
		 */
		public byte getCount() {
			return paramCount;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ParamTOCResponse [paramCount=" + paramCount + ", crc32="
					+ Arrays.toString(crc32) + "]";
		}

	}

	/**
	 * Packet used for sending control set-points for the roll/pitch/yaw/thrust
	 * regulators.
	 */
	public static class CommandPacket extends Request {
		private final float roll;
		private final float pitch;
		private final float yaw;
		private final char thrust;

		/**
		 * Create a new commander packet.
		 * 
		 * @param roll
		 *            (Deg.)
		 * @param pitch
		 *            (Deg.)
		 * @param yaw
		 *            (Deg./s)
		 * @param thrust
		 *            (0-65535)
		 * @param xmode
		 */
		public CommandPacket(float roll, float pitch, float yaw, char thrust,
				boolean xmode) {
			super((byte) 0x30);

			if (xmode) {
				this.pitch = 0.707f * (roll + pitch);
				this.roll = 0.707f * (roll - pitch);
			} else {
				this.pitch = pitch;
				this.roll = roll;
			}
			this.yaw = yaw;
			this.thrust = thrust;

		}

		/**
		 * @return the roll
		 */
		public float getRoll() {
			return roll;
		}

		/**
		 * @return the pitch
		 */
		public float getPitch() {
			return pitch;
		}

		/**
		 * @return the yaw
		 */
		public float getYaw() {
			return yaw;
		}

		/**
		 * @return the thrust
		 */
		public char getThrust() {
			return thrust;
		}

		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.putFloat(roll);
			buffer.putFloat(pitch);
			buffer.putFloat(yaw);
			buffer.putChar(thrust);
		}

		@Override
		protected int getLength() {
			return 3 * 4 + 1 * 2; // 3 floats with size 4, 1 char (= uint16_t)
									// with
									// size 2
		}
	}

	/**
	 * Packet containing Log from CrazyFlie
	 */
	public static class LogTOCRequest extends Request {
		public static final byte CHANNEL = 0;

		private final byte command;
		private final byte item;
		private final byte length;

		/**
		 * Create a new Log TOC CRC and count Request
		 */
		public LogTOCRequest() {
			super(CHANNEL, LOG_PORT);
			this.command = 0x01;
			this.item = 0;
			length = 1;
		}

		/**
		 * Create a new Log TOC item Request
		 * 
		 * @param item
		 */
		public LogTOCRequest(byte item) {
			super(CHANNEL, LOG_PORT);
			this.command = 0x00;
			this.item = item;
			length = 2;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Packet#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(command);
			if (length > 1)
				buffer.put(item);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Packet#getDataByteCount()
		 */
		@Override
		protected int getLength() {
			return length;
		}
	}

	public static class LogTOCItemResponse extends Response {
		public static final byte CMD = 0x00;
		private final byte item;
		private final byte type;
		private final String group;
		private final String name;

		/**
		 * @param data
		 */
		public LogTOCItemResponse(byte[] data) {
			super(data);
			this.item = data[2];
			this.type = data[3];
			int idx = ArrayUtils.indexOf(data, SEPARATOR, 4);
			group = new String(data, 4, idx - 4);
			name = new String(data, idx + 1, data.length - idx - 2);
		}

		/**
		 * @return the item
		 */
		public byte getItem() {
			return item;
		}

		/**
		 * @return the type
		 */
		public byte getType() {
			return type;
		}

		/**
		 * @return the group
		 */
		public String getGroup() {
			return group;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "LogTOCItemResponse [item=" + item + ", type=" + type
					+ ", group=" + group + ", name=" + name + "]";
		}

	}

	public static class LogTOCResponse extends Response {
		public static final byte CMD = 0x01;
		private final byte variables;
		private final byte[] crc32;
		private final byte maxBlocks;
		private final byte maxVariables;

		public LogTOCResponse(byte[] data) {
			super(data);
			this.variables = data[2];
			this.crc32 = Arrays.copyOfRange(data, 3, 6);
			this.maxBlocks = data[7];
			this.maxVariables = data[8];
		}

		/**
		 * @return the variables
		 */
		public byte getCount() {
			return variables;
		}

		/**
		 * @return the crc32
		 */
		public byte[] getCrc32() {
			return crc32;
		}

		/**
		 * @return the maxBlocks
		 */
		public byte getMaxBlocks() {
			return maxBlocks;
		}

		/**
		 * @return the maxVariables
		 */
		public byte getMaxVariables() {
			return maxVariables;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "LogTOCResponse [variables=" + variables + ", crc32="
					+ Arrays.toString(crc32) + ", maxBlocks=" + maxBlocks
					+ ", maxVariables=" + maxVariables + "]";
		}

	}

	public static final class LogSetBlockRequest extends Request {

		public static final byte CONTROL_CREATE_BLOCK = 0x00;
		public static final byte CONTROL_APPEND_BLOCK = 0x01;
		private final byte control;
		private final byte blockId;
		private final LogVariable[] variables;

		public LogSetBlockRequest(byte control, int blockId,
				LogVariable... variables) {
			super(Crtp.LOG_CHANNEL_SETTINGS, Crtp.LOG_PORT);
			this.control = control;
			this.blockId = (byte) blockId;
			if (variables.length * 2 > 30)
				throw new IllegalArgumentException("To many variables");
			this.variables = variables;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(control);
			buffer.put(blockId);
			for (LogVariable v : variables) {
				buffer.put((byte) v.getType());
				buffer.put((byte) v.getId());
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 2 + (variables.length * 2);
		}
	}

	public static final class LogDeleteBlockRequest extends Request {
		public static final byte CONTROL_DELETE_BLOCK = 0x02;
		private final byte blockId;

		public LogDeleteBlockRequest(int blockId) {
			super(Crtp.LOG_CHANNEL_SETTINGS, Crtp.LOG_PORT);
			this.blockId = (byte) blockId;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(CONTROL_DELETE_BLOCK);
			buffer.put(blockId);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 2;
		}
	}

	public static final class LogStartBlockRequest extends Request {
		public static final byte CONTROL_START_BLOCK = 0x03;
		private final byte blockId;
		private final byte period;

		public LogStartBlockRequest(int blockId, int period) {
			super(Crtp.LOG_CHANNEL_SETTINGS, Crtp.LOG_PORT);
			this.blockId = (byte) blockId;
			this.period = (byte) (period / 10);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(CONTROL_START_BLOCK);
			buffer.put(blockId);
			buffer.put(period);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 3;
		}
	}

	public static final class LogStopBlockRequest extends Request {
		public static final byte CONTROL_STOP_BLOCK = 0x04;
		private final byte blockId;

		public LogStopBlockRequest(int blockId) {
			super(Crtp.LOG_CHANNEL_SETTINGS, Crtp.LOG_PORT);
			this.blockId = (byte) blockId;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(CONTROL_STOP_BLOCK);
			buffer.put(blockId);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 2;
		}
	}

	public static final class LogResetRequest extends Request {

		public static final byte CONTROL_RESET = 0x05;

		public LogResetRequest() {
			super(Crtp.LOG_CHANNEL_SETTINGS, Crtp.LOG_PORT);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * se.bitcraze.crazyflie.crtp.Crtp.Request#serializeData(java.nio.ByteBuffer
		 * )
		 */
		@Override
		protected void serializeData(ByteBuffer buffer) {
			buffer.put(CONTROL_RESET);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Request#getLength()
		 */
		@Override
		protected int getLength() {
			return 1;
		}
	}

	public static final class LogSettingsResponse extends Response {
		private final int blockId;
		private final int returnCode;

		public LogSettingsResponse(byte[] data) {
			super(data);
			this.blockId = data[2];
			this.returnCode = data[3];
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return false;
		}

		public int getBlockId() {
			return blockId;
		}

		public int getReturnCode() {
			return returnCode;
		}

	}

	public static final class LogBlockResponse extends Response {

		private final int blockId;
		private final long timestamp;
		private final byte[] data;

		public LogBlockResponse(byte[] data) {
			super(data);
			this.blockId = data[1];
			this.timestamp = 0l;
			this.data = Arrays.copyOfRange(data, 5, data.length);
		}

		/**
		 * @return the blockId
		 */
		public int getBlockId() {
			return blockId;
		}

		/**
		 * @return the timestamp
		 */
		public long getTimestamp() {
			return timestamp;
		}

		/**
		 * @return the data
		 */
		public byte[] getData() {
			return data;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "LogBlockResponse [getPort()=" + getPort()
					+ ", getChannel()=" + getChannel() + ", blockId=" + blockId
					+ ", timestamp=" + timestamp + ", data="
					+ Arrays.toString(data) + "]";
		}

	}

	public static final class ConsoleResponse extends Response {
		public static final byte PORT = 0;

		private final String text;

		/**
		 * @param data
		 */
		public ConsoleResponse(byte[] data) {
			super(data);
			this.text = new String(data, 1, data.length, CHARSET);
		}

		/**
		 * @return the text
		 */
		public String getText() {
			return text;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see se.bitcraze.crazyflie.crtp.Crtp.Response#isAsync()
		 */
		@Override
		public boolean isNotification() {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "[ConsoleResponse]: " + text;
		}

	}
}
