/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package se.bitcraze.crazyflie.util;

public class BytePrinter {

    final static char[] digits = {
    	'0' , '1' , '2' , '3' , '4' , '5' ,
    	'6' , '7' , '8' , '9' , 'A' , 'B' ,
    	'C' , 'D', 'E', 'F'};

    public static final String printHex(byte[] data){
    	return printHex(data, data.length);
    }
    
    public static final String printHex(byte[] data, String separator){
    	return printHex(data, 0, data.length, separator);
    }

    public static final String printHex(byte[] data, int amount){
    	return printHex(data, 0, amount, " ");
    }

    public static final String printHex(byte[] data, int start, int amount){
    	return printHex(data, start, amount, " ");
    }

    public static final String printHex(byte[] data, int start, int amount, String separator){
    	boolean printSep = separator != null  && separator.length() > 0;
		StringBuffer buffer = new StringBuffer(data.length+(printSep ? data.length-1 : 0));
		for (int i = start; i < start+amount; i++) {
			buffer.append(digits[data[i]>>4&0x0F]);
			buffer.append(digits[data[i]&0x0F]);
			if(printSep && i<amount-1){
				buffer.append(separator);
			}
		}
		return buffer.toString();
	}

    public static final String printString(byte[] data){
    	return printString(data, data.length);
    }
    
    public static final String printString(byte[] data, int amount){
    	return printString(data, 0, amount);
    }

    public static final String printString(byte[] data, int start, int amount){
    	if(amount < 0){
    		return null;
    	}
    	return new String(data, start, amount);
    }
}
