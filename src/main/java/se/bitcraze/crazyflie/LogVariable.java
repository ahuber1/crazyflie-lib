/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
package se.bitcraze.crazyflie;

/**
 * @author Andreas Huber
 * 
 */
public class LogVariable extends Element {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4618168958128221909L;
	private static final int[] TYPE_LENGTH = new int[] { 0, 1, 2, 4, 1, 2, 4,
			4, 4 };

	/**
	 * @param id
	 * @param type
	 * @param group
	 * @param name
	 */
	public LogVariable(int id, byte type, String group, String name) {
		super(id, type, group, name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflie.Element#getLength()
	 */
	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Variable [getId()=" + getId() + ", getName()=" + getName()
				+ ", getType()=" + getType() + "]";
	}

}
