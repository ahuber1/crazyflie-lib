/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
package se.bitcraze.crazyflie;

/**
 * @author Andreas Huber
 * 
 */
public class Parameter extends Element {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4496588234739652192L;

	private static final int[] TYPE_LENGTH = new int[] { 1, 2, 4, 8, 0, 0, 4,
			8, 1, 2, 4, 8 };
	private final boolean readOnly;

	/**
	 * @param id
	 * @param type
	 * @param group
	 * @param name
	 */
	public Parameter(int id, int type, String group, String name,
			boolean readOnly) {
		super(id, type, group, name);
		this.readOnly = readOnly;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see se.bitcraze.crazyflie.Element#getLength()
	 */
	@Override
	public int getLength() {
		return TYPE_LENGTH[getType()];
	}

	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parameter [getId()=" + getId() + ", getName()=" + getName()
				+ ", getType()=" + getType() + ", getValue()=" + getValue()
				+ ", readOnly=" + readOnly + "]";
	}
}
