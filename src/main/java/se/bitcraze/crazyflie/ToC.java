/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import se.bitcraze.crazyflie.crtp.CrtpDriver;

/**
 * Basic Table of Content for Parameters and Logging
 * 
 * @author Andreas Huber
 * 
 */
public abstract class ToC<I extends Element> extends ConnectionAdapter {

	private Map<String, I> elements = new HashMap<String, I>();
	protected volatile CrtpDriver driver;
	private int count;

	/**
	 * 
	 */
	public ToC() {
	}

	@Override
	public final void connectionInitiated(CrtpDriver driver) {
		super.connectionInitiated(driver);
		this.driver = driver;
		count = init();
	}

	public void disconnected(CrtpDriver l) {
		release();
		this.driver = null;
	}

	protected abstract int init();

	protected abstract void release();

	protected void add(I element) {
		elements.put(element.getName(), element);
	}

	public I get(String name) {
		if (!elements.containsKey(name)) {
			return null;
		}
		return elements.get(name);
	}

	public boolean contains(String name) {
		return elements.containsKey(name);
	}

	public Collection<String> getNames() {
		return elements.keySet();
	}

	public Collection<I> values() {
		return elements.values();
	}

	public int getCount() {
		return count;
	}

}
