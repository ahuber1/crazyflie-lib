/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.joou.UByte;
import org.joou.UInteger;
import org.joou.ULong;
import org.joou.UShort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.bitcraze.crazyflie.crtp.Crtp;

/**
 * @author Andreas Huber
 * 
 */
public class Parameters extends ToC<Parameter> {

	private static final Logger log = LoggerFactory.getLogger(Parameters.class);

	/**
	 * 
	 */
	public Parameters() {
		super();
	}

	/**
	 * Read the Parameter TOC
	 */
	protected int init() {
		Crtp.ParamTOCResponse response = driver
				.send(new Crtp.ParamTOCRequest());
		log.info("Got Response: {}", response);
		if (response == null) {
			return 0;
		}
		for (byte x = 0, y = (byte) (response.getCount()); x < y; x++) {
			Crtp.ParamTOCItemResponse item = driver
					.send(new Crtp.ParamTOCRequest(x));
			if (item == null)
				continue;
			Parameter param = createParameter(item);
			if (param == null)
				continue;
			add(param);
			Crtp.ParamReadResponse r = driver.send(new Crtp.ParamReadRequest(
					(byte) x));
			if (r != null && ArrayUtils.isNotEmpty(r.getValue())) {
				setParameterValue(param, r.getValue());
			}
			log.info("Register Parameter: {}", param);

		}
		return response.getCount();

	}

	@Override
	protected void release() {

	}

	public Parameter read(String name) {
		Parameter param = get(name);
		if (param == null) {
			log.warn("No Parameter {} found in {}", name, getNames());
			return null;
		}
		Crtp.ParamReadResponse r = driver.send(new Crtp.ParamReadRequest(param
				.getId()));
		if (r != null && ArrayUtils.isNotEmpty(r.getValue())) {
			setParameterValue(param, r.getValue());
		}
		return param;
	}

	public boolean set(String name, Object value) {
		if (!contains(name)) {
			log.warn("No Parameter {} found in {}", name, getNames());
			return false;
		}
		Parameter p = get(name);
		byte[] data = convertValue(p, value);
		Crtp.ParamWriteResponse response = driver
				.send(new Crtp.ParamWriteRequest(p.getId(), data));
		if (response == null)
			return false;
		setParameterValue(p, response.getValue());
		return true;
	}

	private static final Parameter createParameter(
			Crtp.ParamTOCItemResponse item) {
		switch (item.getType()) {
		case 0:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 1:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 2:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 3:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 6:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 7:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 8:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 9:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 10:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		case 11:
			return new Parameter(item.getItem(), item.getType(),
					item.getGroup(), item.getName(), item.isReadOnly());
		default:
			break;
		}
		return null;
	}

	private static final void setParameterValue(Parameter parameter, byte[] data) {
		ByteBuffer buf = ByteBuffer.wrap(data).order(Crtp.BYTE_ORDER);
		switch (parameter.getType()) {
		case 0:
			parameter.setValue(buf.get());
			break;
		case 1:
			parameter.setValue(buf.getShort());
			break;
		case 2:
			parameter.setValue(buf.getInt());
			break;
		case 3:
			parameter.setValue(buf.getLong());
			break;
		case 6:
			parameter.setValue(buf.getFloat());
			break;
		case 7:
			parameter.setValue(buf.getDouble());
			break;
		case 8:
			parameter.setValue(UByte.valueOf(buf.get()));
			break;
		case 9:
			parameter.setValue(UShort.valueOf(buf.getShort()));
			break;
		case 10:
			parameter.setValue(UInteger.valueOf(buf.getInt()));
			break;
		case 11:
			parameter.setValue(ULong.valueOf(buf.getLong()));
			break;
		default:
			break;
		}
	}

	private static final byte[] convertValue(Parameter parameter, Object value) {
		ByteBuffer buf = ByteBuffer.allocate(parameter.getLength()).order(
				Crtp.BYTE_ORDER);

		switch (parameter.getType()) {
		case 0:
			buf.put((Byte) value);
			break;
		case 1:
			buf.putShort((Short) value);
			break;
		case 2:
			buf.putInt((Integer) value);
			break;
		case 3:
			buf.putLong((Long) value);
			break;
		case 6:
			buf.putFloat((Float) value);
			break;
		case 7:
			buf.putDouble((Double) value);
			break;
		case 8:
			buf.put(((UByte) value).byteValue());
			break;
		case 9:
			buf.putShort(((UShort) value).shortValue());
			break;
		case 10:
			buf.putInt(((UInteger) value).intValue());
			break;
		case 11:
			buf.putLong(((ULong) value).longValue());
			break;
		default:
			break;
		}
		return buf.array();
	}
}
